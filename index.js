const express = require("express");

const mongoose = require("mongoose");

const app = express();
const port = 4001;

mongoose.connect("mongodb+srv://admin:admin@zuitt-course-booking.kegoxwu.mongodb.net/b244-to-do?retryWrites=true&w=majority", 
	{
		useNewUrlParser : true,
		useUnifiedTopology : true
	}
);

let db = mongoose.connection;

db.on("error", console.error.bind(console, "connection error"));


db.once("open", ()=>{console.log("We're connected to the database")});

const taskSchema = new mongoose.Schema({
	name : String,
	password: String,
	status : {
		type : String,
	}
});


const Task = mongoose.model("Task", taskSchema);

app.use(express.json());

app.use(express.urlencoded({extended:true}));


app.post("/signup", (req, res) =>{
	Task.findOne({name : req.body.name}, (err, result) =>{
		if(result != null && result.name.password == req.body.name.password){
			return res.send("already exist");
		}else{
			let newTask = new Task({
				name : req.body.name,
				password: req.body.password
			});
			newTask.save((saveErr, savedTask) => {
				if(saveErr){
					return console.error(saveErr);
				}else{
					return res.status(201).send("New user registered");
				}
			})
		}
	})
})
app.get("/signup", (req, res) =>{
	Task.find({}, (err, result) =>{
	if(err){
			return console.error(err);
		}else{
			return res.status(200).json({data:result});
		}
	})
})


app.listen(port, () => {console.log(`Server running at port ${port}`)});